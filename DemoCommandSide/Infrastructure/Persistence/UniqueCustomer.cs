﻿namespace DemoCommandSide.Infrastructure.Persistence
{
    public class UniqueCustomer
    {
        public required Guid Id { get; init; }
        public required string Phone { get; set; }
        public required string Email { get; set; }
    }
}
