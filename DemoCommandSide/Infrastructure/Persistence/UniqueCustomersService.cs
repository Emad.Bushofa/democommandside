﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DemoCommandSide.Infrastructure.Persistence
{
    public class UniqueCustomersService(ApplicationDbContext context) : ICustomersQueriesService
    {
        private readonly ApplicationDbContext _context = context;

        public async Task EnsurePhoneAndEmailAreUniqueAsync(string phone, string email, CancellationToken cancellationToken)
        {
            var uniqueCustomer = await _context.UniqueCustomers.FirstOrDefaultAsync(
                x => (x.Phone == phone || x.Email == email),
                cancellationToken
            );

            if (uniqueCustomer is not null)
            {
                if (uniqueCustomer.Email == email)
                    throw new AlreadyExistsException("Email is already taken.");

                if (uniqueCustomer.Phone == phone)
                    throw new AlreadyExistsException("Phone is already taken.");
            }
        }

        public async Task EnsurePhoneAndEmailAreUniqueAsync(Guid customerId, string phone, string email, CancellationToken cancellationToken)
        {
            var uniqueCustomer = await _context.UniqueCustomers.FirstOrDefaultAsync(
                x => (x.Phone == phone || x.Email == email) && x.Id != customerId,
                cancellationToken
            );

            if (uniqueCustomer is not null)
            {
                if (uniqueCustomer.Email == email)
                    throw new AlreadyExistsException("Email is already taken.");

                if (uniqueCustomer.Phone == phone)
                    throw new AlreadyExistsException("Phone is already taken.");
            }
        }
    }
}
