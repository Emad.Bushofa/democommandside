﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Domain;
using DemoCommandSide.Events;
using DemoCommandSide.Infrastructure.MessageBus;
using Microsoft.EntityFrameworkCore;

namespace DemoCommandSide.Infrastructure.Persistence
{
    public class EventStore(ApplicationDbContext context, ServiceBusPublisher publisher) : IEventStore
    {
        private readonly ApplicationDbContext _context = context;
        private readonly ServiceBusPublisher _publisher = publisher;

        public Task<List<Event>> GetAllAsync(Guid aggregateId, CancellationToken cancellationToken) =>
            _context.Events
                .Where(x => x.AggregateId == aggregateId)
                .OrderBy(x => x.Id)
                .ToListAsync(cancellationToken);

        public async Task CommitAsync(IAggregate customer, CancellationToken cancellationToken)
        {
            await UpdateUniqueCustomers((Customer)customer, cancellationToken);

            var events = customer.GetUncommittedEvents();

            var messages = events.Select(x => new OutboxMessage(x));

            await _context.Events.AddRangeAsync(events, cancellationToken);
            await _context.OutboxMessages.AddRangeAsync(messages, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            _publisher.StartPublishing();
        }

        private async Task UpdateUniqueCustomers(Customer customer, CancellationToken cancellationToken)
        {
            var uniqueCustomer = await _context.UniqueCustomers.FindAsync(customer.Id);

            if (uniqueCustomer is null)
            {
                await _context.UniqueCustomers.AddAsync(new UniqueCustomer
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    Phone = customer.Phone
                }, cancellationToken);
            }
            else
            {
                uniqueCustomer.Email = customer.Email;
                uniqueCustomer.Phone = customer.Phone;
            }
        }
    }
}
