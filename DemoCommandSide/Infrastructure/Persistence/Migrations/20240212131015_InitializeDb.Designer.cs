﻿// <auto-generated />
using System;
using DemoCommandSide.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace DemoCommandSide.Infrastructure.Persistence.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20240212131015_InitializeDb")]
    partial class InitializeDb
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("DemoCommandSide.Events.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<Guid>("AggregateId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("DateTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("EventType")
                        .IsRequired()
                        .HasMaxLength(128)
                        .HasColumnType("nvarchar(128)");

                    b.Property<int>("Sequence")
                        .HasColumnType("int");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AggregateId", "Sequence")
                        .IsUnique();

                    b.ToTable("Events");

                    b.HasDiscriminator<string>("EventType").HasValue("Event");

                    b.UseTphMappingStrategy();
                });

            modelBuilder.Entity("DemoCommandSide.Infrastructure.Persistence.UniqueCustomer", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.HasIndex("Phone")
                        .IsUnique();

                    b.ToTable("UniqueCustomers");
                });

            modelBuilder.Entity("DemoCommandSide.Events.CustomerContactInfoUpdated", b =>
                {
                    b.HasBaseType("DemoCommandSide.Events.Event");

                    b.Property<string>("Data")
                        .ValueGeneratedOnUpdateSometimes()
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("Data");

                    b.HasDiscriminator().HasValue("CustomerContactInfoUpdated");
                });

            modelBuilder.Entity("DemoCommandSide.Events.CustomerDeleted", b =>
                {
                    b.HasBaseType("DemoCommandSide.Events.Event");

                    b.Property<string>("Data")
                        .ValueGeneratedOnUpdateSometimes()
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("Data");

                    b.HasDiscriminator().HasValue("CustomerDeleted");
                });

            modelBuilder.Entity("DemoCommandSide.Events.CustomerNameChanged", b =>
                {
                    b.HasBaseType("DemoCommandSide.Events.Event");

                    b.Property<string>("Data")
                        .ValueGeneratedOnUpdateSometimes()
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("Data");

                    b.HasDiscriminator().HasValue("CustomerNameChanged");
                });

            modelBuilder.Entity("DemoCommandSide.Events.CustomerRegistered", b =>
                {
                    b.HasBaseType("DemoCommandSide.Events.Event");

                    b.Property<string>("Data")
                        .ValueGeneratedOnUpdateSometimes()
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("Data");

                    b.HasDiscriminator().HasValue("CustomerRegistered");
                });
#pragma warning restore 612, 618
        }
    }
}
