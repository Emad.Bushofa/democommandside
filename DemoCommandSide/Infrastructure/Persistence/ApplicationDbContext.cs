﻿using DemoCommandSide.Events;
using DemoCommandSide.Infrastructure.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;

namespace DemoCommandSide.Infrastructure.Persistence
{
    public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<OutboxMessage> OutboxMessages { get; set; }
        public DbSet<UniqueCustomer> UniqueCustomers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UniqueCustomerConfiguration());
            modelBuilder.ApplyConfiguration(new OutboxMessageConfigurations());
            modelBuilder.ApplyConfiguration(new BaseEventConfigurations());
            modelBuilder.ApplyConfiguration(new GenericEventConfiguration<CustomerRegistered, CustomerRegisteredData>());
            modelBuilder.ApplyConfiguration(new GenericEventConfiguration<CustomerNameChanged, CustomerNameChangedData>());
            modelBuilder.ApplyConfiguration(new GenericEventConfiguration<CustomerContactInfoUpdated, CustomerContactInfoUpdatedData>());
            modelBuilder.ApplyConfiguration(new GenericEventConfiguration<CustomerDeleted, object>());
        }
    }
}
