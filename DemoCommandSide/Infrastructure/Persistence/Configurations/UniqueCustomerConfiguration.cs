﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoCommandSide.Infrastructure.Persistence.Configurations
{
    public class UniqueCustomerConfiguration : IEntityTypeConfiguration<UniqueCustomer>
    {
        public void Configure(EntityTypeBuilder<UniqueCustomer> builder)
        {
            builder.HasIndex(e => e.Email).IsUnique();
            builder.HasIndex(e => e.Phone).IsUnique();
        }
    }
}
