using Azure.Messaging.ServiceBus;
using DemoCommandSide.Abstraction;
using DemoCommandSide.Infrastructure.MessageBus;
using DemoCommandSide.Infrastructure.Persistence;
using DemoCommandSide.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddGrpc();
builder.Services.AddMediatR(o => o.RegisterServicesFromAssemblyContaining<Program>());
builder.Services.AddDbContext<ApplicationDbContext>(o => o.UseInMemoryDatabase("Customers"), ServiceLifetime.Transient);
builder.Services.AddScoped<IEventStore, EventStore>();
builder.Services.AddScoped<ICustomersQueriesService, UniqueCustomersService>();
builder.Services.AddSingleton(new ServiceBusClient(""));
builder.Services.AddSingleton<ServiceBusPublisher>();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<CustomersService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
