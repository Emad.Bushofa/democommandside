﻿namespace DemoCommandSide.Events
{
    public record CustomerDeleted(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        string UserId,
        int Version
    ) : Event<object>(AggregateId, Sequence, DateTime, new(), UserId, Version);
}
