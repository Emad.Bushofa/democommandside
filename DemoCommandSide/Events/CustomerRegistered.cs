﻿using DemoCommandSide.Domain;

namespace DemoCommandSide.Events
{
    public record CustomerRegistered(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        CustomerRegisteredData Data,
        string UserId,
        int Version
    ) : Event<CustomerRegisteredData>(AggregateId, Sequence, DateTime, Data, UserId, Version);

    public record CustomerRegisteredData(
        string FirstName,
        string? LastName,
        string Email,
        string Phone,
        Address Address
    );
}
