﻿namespace DemoCommandSide.Events
{
    public interface IEvent
    {
        Guid AggregateId { get; }
        int Sequence { get; }
        DateTime DateTime { get; }
        string UserId { get; }
        int Version { get; }
        string Type { get; }
        dynamic Data { get; }
    }
}
