﻿namespace DemoCommandSide.Events
{
    public record CustomerNameChanged(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        CustomerNameChangedData Data,
        string UserId,
        int Version
    ) : Event<CustomerNameChangedData>(AggregateId, Sequence, DateTime, Data, UserId, Version);

    public record CustomerNameChangedData(
        string FirstName,
        string? LastName
    );
}
