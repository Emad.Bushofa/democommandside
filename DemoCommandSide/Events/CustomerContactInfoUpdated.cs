﻿namespace DemoCommandSide.Events
{
    public record CustomerContactInfoUpdated(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        CustomerContactInfoUpdatedData Data,
        string UserId,
        int Version
    ) : Event<CustomerContactInfoUpdatedData>(AggregateId, Sequence, DateTime, Data, UserId, Version);

    public record CustomerContactInfoUpdatedData(
        string Email,
        string Phone
    );
}
