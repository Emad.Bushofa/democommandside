﻿namespace DemoCommandSide.Events
{
    public abstract record Event(
        int Id,
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        string UserId,
        int Version
    ) : IEvent
    {
        public string Type => GetType().Name;
        dynamic IEvent.Data => ((dynamic)this).Data;
    }

    public abstract record Event<T>(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        T Data,
        string UserId,
        int Version
    ) : Event(Id: default, AggregateId, Sequence, DateTime, UserId, Version);
}
