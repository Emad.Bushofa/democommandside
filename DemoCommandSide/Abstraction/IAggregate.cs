﻿using DemoCommandSide.Events;

namespace DemoCommandSide.Abstraction
{
    public interface IAggregate
    {
        Guid Id { get; }
        int Sequence { get; }
        IReadOnlyList<Event> GetUncommittedEvents();
        void MarkChangesAsCommitted();
    }
}
