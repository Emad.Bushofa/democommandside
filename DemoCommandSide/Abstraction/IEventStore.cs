﻿using DemoCommandSide.Events;

namespace DemoCommandSide.Abstraction
{
    public interface IEventStore
    {
        Task<List<Event>> GetAllAsync(Guid aggregateId, CancellationToken cancellationToken);
        Task CommitAsync(IAggregate aggregate, CancellationToken cancellationToken);
    }
}
