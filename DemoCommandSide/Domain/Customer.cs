﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Commands.ChangeName;
using DemoCommandSide.Commands.Register;
using DemoCommandSide.Commands.UpdateContactInfo;
using DemoCommandSide.Events;
using DemoCommandSide.Exceptions;
using DemoCommandSide.Extensions;

namespace DemoCommandSide.Domain
{
    public class Customer : Aggregate<Customer>, IAggregate
    {
        public static Customer Register(RegisterCustomerCommand command)
        {
            var customer = new Customer();
            customer.ApplyNewChange(command.ToEvent());
            return customer;
        }

        public bool IsDeleted { get; private set; }
        public int TotalFirstNameChangesToday { get; private set; }
        public int TotalLastNameChangesToday { get; private set; }
        public string Email { get; private set; } = string.Empty;
        public string Phone { get; private set; } = string.Empty;
        public string FirstName { get; private set; } = string.Empty;
        public string? LastName { get; private set; }

        public void ChangeName(ChangeCustomerNameCommand command)
        {
            if (IsDeleted)
                throw new NotFoundException("Customer not found");

            if (TotalFirstNameChangesToday >= 3)
                throw new BusinessRuleViolationException("You can only change your first name 3 times per day");

            if (TotalLastNameChangesToday >= 5)
                throw new BusinessRuleViolationException("You can only change your last name 5 times per day");

            ApplyNewChange(command.ToEvent(NextSequence));
        }

        public void UpdateContactInfo(UpdateCustomerContactInfoCommand command)
        {
            if (IsDeleted)
                throw new NotFoundException("Customer not found");

            ApplyNewChange(new CustomerContactInfoUpdated(
                AggregateId: command.Id,
                Sequence: Sequence + 1,
                DateTime: DateTime.UtcNow,
                Data: new CustomerContactInfoUpdatedData(
                    Email: command.Email,
                    Phone: command.Phone
                ),
                UserId: command.UserId,
                Version: 1
            ));
        }

        protected override void Mutate(Event @event)
        {
            switch (@event)
            {
                case CustomerRegistered e:
                    Mutate(e);
                    break;
                case CustomerNameChanged e:
                    Mutate(e);
                    break;
                case CustomerContactInfoUpdated e:
                    Mutate(e);
                    break;
                case CustomerDeleted e:
                    Mutate(e);
                    break;
            }
        }

        public void Mutate(CustomerRegistered @event)
        {
            Email = @event.Data.Email;
            Phone = @event.Data.Phone;
            FirstName = @event.Data.FirstName;
            LastName = @event.Data.LastName;
        }

        public void Mutate(CustomerNameChanged @event)
        {
            if (@event.DateTime.Date == DateTime.UtcNow.Date)
            {
                if (FirstName != @event.Data.FirstName)
                {
                    TotalFirstNameChangesToday++;
                }

                if (LastName != @event.Data.LastName)
                {
                    TotalLastNameChangesToday++;
                }
            }
            else
            {
                TotalFirstNameChangesToday = 1;
                TotalLastNameChangesToday = 1;
            }

            FirstName = @event.Data.FirstName;
            LastName = @event.Data.LastName;
        }

        public void Mutate(CustomerContactInfoUpdated @event)
        {
            Email = @event.Data.Email;
            Phone = @event.Data.Phone;
        }

        public void Mutate(CustomerDeleted _)
        {
            IsDeleted = true;
        }
    }
}
