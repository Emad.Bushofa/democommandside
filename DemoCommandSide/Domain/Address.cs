﻿namespace DemoCommandSide.Domain
{
    public record Address(string Street, string City, string State, string Zip);
}
