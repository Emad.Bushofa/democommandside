﻿using DemoCommandSide.Commands.ChangeName;
using DemoCommandSide.Commands.Register;
using DemoCommandSide.Commands.UpdateContactInfo;
using DemoCommandSide.Events;
using DemoCommandSide.Exceptions;

namespace DemoCommandSide.Domain
{
    public class OldCustomer
    {
        public static OldCustomer LoadFromHistory(List<Event> events)
        {
            var customer = new OldCustomer();
            foreach (var @event in events)
            {
                customer.ApplyChanges(@event, false);
            }
            return customer;
        }

        private void ApplyChanges(Event @event, bool isNew = true)
        {
            Id = @event.AggregateId;
            Sequence = @event.Sequence;

            if (isNew)
                UncommitedEvents.Add(@event);

            if (@event is CustomerRegistered customerRegistered)
            {
                Mutate(customerRegistered);
            }

            if (@event is CustomerNameChanged customerNameChanged)
            {
                Mutate(customerNameChanged);
            }

            if (@event is CustomerContactInfoUpdated customerContactInfoUpdated)
            {
                Mutate(customerContactInfoUpdated);
            }

            if (@event is CustomerDeleted customerDeleted)
            {
                Mutate(customerDeleted);
            }
        }


        public static OldCustomer Register(RegisterCustomerCommand command)
        {
            var customer = new OldCustomer();

            customer.ApplyChanges(new CustomerRegistered(
                AggregateId: Guid.NewGuid(),
                Sequence: 1,
                DateTime: DateTime.UtcNow,
                Data: new CustomerRegisteredData(
                    FirstName: command.FirstName,
                    LastName: command.LastName,
                    Email: command.Email,
                    Phone: command.Phone,
                    Address: command.Address
                ),
                UserId: command.UserId,
                Version: 1
            ));
            return customer;
        }

        public Guid Id { get; private set; }
        public int Sequence { get; private set; }
        public bool IsDeleted { get; private set; }
        public int TotalFirstNameChangesToday { get; private set; }
        public int TotalLastNameChangesToday { get; private set; }
        public string Email { get; private set; } = string.Empty;
        public string Phone { get; private set; } = string.Empty;
        public string FirstName { get; private set; } = string.Empty;
        public string? LastName { get; private set; }

        public List<Event> UncommitedEvents { get; } = [];

        public void ChangeName(ChangeCustomerNameCommand command)
        {
            if (IsDeleted)
                throw new NotFoundException("Customer not found");

            if (TotalFirstNameChangesToday >= 3)
                throw new BusinessRuleViolationException("You can only change your first name 3 times per day");

            if (TotalLastNameChangesToday >= 5)
                throw new BusinessRuleViolationException("You can only change your last name 5 times per day");

            ApplyChanges(new CustomerNameChanged(
                AggregateId: command.Id,
                Sequence: Sequence + 1,
                DateTime: DateTime.UtcNow,
                Data: new CustomerNameChangedData(
                    FirstName: command.FirstName,
                    LastName: command.LastName
                ),
                UserId: command.UserId,
                Version: 1
            ));
        }

        public void UpdateContactInfo(UpdateCustomerContactInfoCommand command)
        {
            if (IsDeleted)
                throw new NotFoundException("Customer not found");

            ApplyChanges(new CustomerContactInfoUpdated(
                AggregateId: command.Id,
                Sequence: Sequence + 1,
                DateTime: DateTime.UtcNow,
                Data: new CustomerContactInfoUpdatedData(
                    Email: command.Email,
                    Phone: command.Phone
                ),
                UserId: command.UserId,
                Version: 1
            ));
        }

        public void Mutate(CustomerRegistered @event)
        {
            Email = @event.Data.Email;
            Phone = @event.Data.Phone;
            FirstName = @event.Data.FirstName;
            LastName = @event.Data.LastName;
        }

        public void Mutate(CustomerNameChanged @event)
        {
            if (@event.DateTime.Date == DateTime.UtcNow.Date)
            {
                if (FirstName != @event.Data.FirstName)
                {
                    TotalFirstNameChangesToday++;
                }

                if (LastName != @event.Data.LastName)
                {
                    TotalLastNameChangesToday++;
                }
            }
            else
            {
                TotalFirstNameChangesToday = 1;
                TotalLastNameChangesToday = 1;
            }

            FirstName = @event.Data.FirstName;
            LastName = @event.Data.LastName;
        }

        public void Mutate(CustomerContactInfoUpdated @event)
        {
            Email = @event.Data.Email;
            Phone = @event.Data.Phone;
        }

        public void Mutate(CustomerDeleted _)
        {
            IsDeleted = true;
        }
    }
}
