﻿using DemoCommandSide.Domain;
using MediatR;

namespace DemoCommandSide.Commands.Register
{
    public class RegisterCustomerCommand : IRequest<Guid>
    {
        public required string UserId { get; init; }
        public required string FirstName { get; init; }
        public required string? LastName { get; init; }
        public required string Email { get; init; }
        public required string Phone { get; init; }
        public required Address Address { get; init; }
    }
}
