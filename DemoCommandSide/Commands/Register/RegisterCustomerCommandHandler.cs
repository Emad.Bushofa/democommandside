﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Domain;
using MediatR;

namespace DemoCommandSide.Commands.Register
{
    public class RegisterCustomerCommandHandler(IEventStore eventStore, ICustomersQueriesService queriesService) : IRequestHandler<RegisterCustomerCommand, Guid>
    {
        private readonly IEventStore _eventStore = eventStore;
        private readonly ICustomersQueriesService _queriesService = queriesService;

        public async Task<Guid> Handle(RegisterCustomerCommand command, CancellationToken cancellationToken)
        {
            await _queriesService.EnsurePhoneAndEmailAreUniqueAsync(command.Phone, command.Email, cancellationToken);

            var customer = Customer.Register(command);

            await _eventStore.CommitAsync(customer, cancellationToken);

            return customer.Id;
        }
    }
}
