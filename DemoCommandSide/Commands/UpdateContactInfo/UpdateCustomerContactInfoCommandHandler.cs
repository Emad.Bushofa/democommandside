﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Domain;
using DemoCommandSide.Exceptions;
using MediatR;

namespace DemoCommandSide.Commands.UpdateContactInfo
{
    public class UpdateCustomerContactInfoCommandHandler(IEventStore eventStore, ICustomersQueriesService queriesService) : IRequestHandler<UpdateCustomerContactInfoCommand, Guid>
    {
        private readonly IEventStore _eventStore = eventStore;
        private readonly ICustomersQueriesService _queriesService = queriesService;

        public async Task<Guid> Handle(UpdateCustomerContactInfoCommand command, CancellationToken cancellationToken)
        {
            await _queriesService.EnsurePhoneAndEmailAreUniqueAsync(command.Id, command.Phone, command.Email, cancellationToken);

            var events = await _eventStore.GetAllAsync(command.Id, cancellationToken);

            if (events.Count == 0)
                throw new NotFoundException("Customer not found");

            var customer = Customer.LoadFromHistory(events);
            customer.UpdateContactInfo(command);

            await _eventStore.CommitAsync(customer, cancellationToken);

            return command.Id;
        }
    }
}
