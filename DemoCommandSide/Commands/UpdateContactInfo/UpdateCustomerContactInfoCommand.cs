﻿using MediatR;

namespace DemoCommandSide.Commands.UpdateContactInfo
{
    public class UpdateCustomerContactInfoCommand : IRequest<Guid>
    {
        public required Guid Id { get; init; }
        public required string UserId { get; init; }
        public required string Email { get; init; }
        public required string Phone { get; init; }
    }
}
