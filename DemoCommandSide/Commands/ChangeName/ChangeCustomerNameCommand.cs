﻿using MediatR;

namespace DemoCommandSide.Commands.ChangeName
{
    public class ChangeCustomerNameCommand : IRequest<Guid>
    {
        public required Guid Id { get; init; }
        public required string UserId { get; init; }
        public required string FirstName { get; init; }
        public required string? LastName { get; init; }
    }
}
