﻿using DemoCommandSide.Abstraction;
using DemoCommandSide.Domain;
using DemoCommandSide.Exceptions;
using MediatR;

namespace DemoCommandSide.Commands.ChangeName
{
    public class ChangeCustomerNameCommandHandler(IEventStore eventStore) : IRequestHandler<ChangeCustomerNameCommand, Guid>
    {
        private readonly IEventStore _eventStore = eventStore;

        public async Task<Guid> Handle(ChangeCustomerNameCommand command, CancellationToken cancellationToken)
        {
            var events = await _eventStore.GetAllAsync(command.Id, cancellationToken);

            if (events.Count == 0)
                throw new NotFoundException("Customer not found");

            var customer = Customer.LoadFromHistory(events);

            customer.ChangeName(command);

            await _eventStore.CommitAsync(customer, cancellationToken);

            return command.Id;
        }
    }
}
