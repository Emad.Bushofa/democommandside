using DemoCommandSide.Extensions;
using Grpc.Core;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Polly;

namespace DemoCommandSide.Services
{
    public class CustomersService(IMediator mediator, ILogger<CustomersService> logger) : Customers.CustomersBase
    {
        private readonly IMediator _mediator = mediator;
        private readonly AsyncPolicy _policy = ConfigureRetries(logger);

        public override async Task<Response> Register(RegisterRequest request, ServerCallContext context)
        {
            var command = request.ToCommand();

            var id = await _policy.ExecuteAsync(() => _mediator.Send(command));

            return new Response()
            {
                Id = id.ToString()
            };
        }

        private static AsyncPolicy ConfigureRetries(ILogger logger) =>
            Policy.Handle<DbUpdateException>()
                .WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(3),
                }, onRetry: (exception, _, attempt, _) => logger.LogWarning(exception, "Call failed, Retry attempt: {RetryAttempt}", attempt));
    }
}
