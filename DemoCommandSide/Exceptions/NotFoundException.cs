﻿namespace DemoCommandSide.Exceptions
{
    public class NotFoundException(string message) : Exception(message)
    {
    }
}
