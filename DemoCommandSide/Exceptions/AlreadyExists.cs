﻿namespace DemoCommandSide.Exceptions
{
    public class AlreadyExistsException(string message) : Exception(message)
    {
    }
}
