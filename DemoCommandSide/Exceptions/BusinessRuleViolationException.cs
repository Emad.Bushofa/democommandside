﻿namespace DemoCommandSide.Exceptions
{
    public class BusinessRuleViolationException(string message) : Exception(message)
    {
    }
}
