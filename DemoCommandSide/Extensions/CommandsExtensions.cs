﻿using DemoCommandSide.Commands.Register;
using DemoCommandSide.Domain;

namespace DemoCommandSide.Extensions
{
    public static class CommandsExtensions
    {
        public static RegisterCustomerCommand ToCommand(this RegisterRequest request)
            => new()
            {
                UserId = request.UserId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Phone = request.Phone,
                Address = new Address(
                    Street: request.Address.Street,
                    City: request.Address.City,
                    State: request.Address.State,
                    Zip: request.Address.Zip
                ),
            };
    }
}
