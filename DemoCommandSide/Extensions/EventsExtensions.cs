﻿using DemoCommandSide.Commands.ChangeName;
using DemoCommandSide.Commands.Register;
using DemoCommandSide.Events;

namespace DemoCommandSide.Extensions
{
    public static class EventsExtensions
    {
        public static CustomerRegistered ToEvent(this RegisterCustomerCommand command) => new(
                AggregateId: Guid.NewGuid(),
                Sequence: 1,
                DateTime: DateTime.UtcNow,
                Data: new CustomerRegisteredData(
                    FirstName: command.FirstName,
                    LastName: command.LastName,
                    Email: command.Email,
                    Phone: command.Phone,
                    Address: command.Address
                ),
                UserId: command.UserId,
                Version: 1
            );

        public static CustomerNameChanged ToEvent(this ChangeCustomerNameCommand command, int sequence) => new(
                AggregateId: command.Id,
                Sequence: sequence,
                DateTime: DateTime.UtcNow,
                Data: new CustomerNameChangedData(
                    FirstName: command.FirstName,
                    LastName: command.LastName
                ),
                UserId: command.UserId,
                Version: 1
            );
    }
}
